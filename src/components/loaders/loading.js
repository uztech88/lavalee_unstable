import React, { PropTypes } from 'react';

export default class loader extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      show: props.show ? props.show : false,
    };

    this.generateLoader = this.generateLoader.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({show : nextProps.show});
  }

  generateLoader() {
    if(this.state.show){
      return(
        <div className="loader-body">
          <div className="loader-container">
            <div class="dizzy-gillespie"></div>
          </div>
        </div>
      );
    }
  }

  render() {
    return (
      {this.generateLoader()}
    );
  }
}

loginErrorDialog.propTypes = {
  show: PropTypes.bool,
};
