import React,  { PropTypes } from 'react';
import Drawer from 'material-ui/Drawer';
import {spacing, typography} from 'material-ui/styles';
import {white, black} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import {Link} from 'react-router';
import Avatar from 'material-ui/Avatar';
import GlobalStyles from "../styles";

const LeftDrawer = (props) => {
  let { navDrawerOpen } = props;

  const styles = {
    drawer: {
      backgroundColor: GlobalStyles.appColors.grey,
    },
    logo: {
      cursor: 'pointer',
      fontSize: 22,
      color: typography.textFullWhite,
      lineHeight: `${spacing.desktopKeylineIncrement}px`,
      fontWeight: typography.fontWeightLight,
      backgroundColor: GlobalStyles.appColors.grey,
      paddingLeft: 40,
      height: 56,
    },
    menuItem: {
      color: white,
      fontSize: 15,
      marginTop: 10,
      marginBottom: 10,
    },
    link: {
      paddingLeft: 50,
    },
    avatar: {
      div: {
        padding: '15px 0 20px 15px',
        backgroundColor: GlobalStyles.appColors.cyan,
        height: 45
      },
      icon: {
        float: 'left',
        display: 'block',
        marginRight: 15,
        boxShadow: '0px 0px 0px 8px rgba(0,0,0,0.2)'
      },
      span: {
        paddingTop: 12,
        display: 'block',
        color: 'white',
        fontWeight: 300,
        fontSize: 20,
        marginLeft: 10,
        textShadow: '1px 1px #444'
      }
    }
  };

  return (
    <Drawer
      docked={true}
      style = {styles.drawer}
      open={navDrawerOpen}>
        <div style={styles.logo}>
          Lavalee Op Panel
        </div>
        <div style={styles.avatar.div}>
          <Avatar src="https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/8/005/0b0/1b2/1040945.jpg"
                  size={50}
                  style={styles.avatar.icon}/>
          <span style={styles.avatar.span}>{props.username}</span>
        </div>
        <div style= {styles.MenuItemContainer}>
          {props.menus.map((menu, index) =>
            <MenuItem
              key={index}
              style={styles.menuItem}
              primaryText={menu.text}
              leftIcon={menu.icon}
              containerElement={<Link to={menu.link}/>}
            />
          )}
        </div>
    </Drawer>
  );
};

LeftDrawer.propTypes = {
  navDrawerOpen: PropTypes.bool,
  menus: PropTypes.array,
  username: PropTypes.string,
};

export default LeftDrawer;
