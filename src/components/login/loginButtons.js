import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import Help from 'material-ui/svg-icons/action/help';
import {grey500} from 'material-ui/styles/colors';
import { Link } from 'react-router';

const LoginButtons = () => {

const styles = {
  buttonsDiv: {
    textAlign: 'center',
    padding: 10
  },
  flatButton: {
    color: grey500
  },
};

return(
  <div style={styles.buttonsDiv}>
    <Link to="/recovery_mail">
      <FlatButton
        label="Forgot Password?"
        style={styles.flatButton}
        icon={<Help />}
      />
    </Link>
  </div>
);
};

// LoginButtons.propTypes = {

// };

export default LoginButtons;