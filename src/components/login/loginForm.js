import React, {PropTypes} from 'react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import {grey500} from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';
// import {Link} from 'react-router';
import LoginButtons from './loginButtons';
import ErrorDialog from '../dialogs/ErrorDialog';

const LoginForm = (props) => {

const {
  action,
  errors,
  onChangeField,
  loginFailedMessage,
  loginFailed,
  closeDialog
} = props;

const styles = {
  loginContainer: {
      minWidth: 320,
      maxWidth: 400,
      height: 'auto',
      position: 'absolute',
      top: '20%',
      left: 0,
      right: 0,
      margin: 'auto'
  },
  paper: {
    padding: 20,
    overflow: 'auto'
  },
  checkRemember: {
    style: {
      float: 'left',
      maxWidth: 180,
      paddingTop: 5
    },
    labelStyle: {
      color: grey500
    },
    iconStyle: {
      color: grey500,
      borderColor: grey500,
      fill: grey500
    }
  },
  loginBtn: {
    float: 'right'
  },
};

const dialogError = () => {
  if(loginFailed){
    console.log("loginErrorDialog hit");
    return <ErrorDialog open = {loginFailed}
    message = {loginFailedMessage} closeDialog = {closeDialog} />;
  }
};

// const loader = () => {
//   if(loginFailed){
//     console.log("loginErrorDialog hit");
//     return <LoginErrorDialog open = {loginFailed}
//     message = {loginFailedMessage} closeDialog = {closeDialog} />;
//   }
// };

return(
 <div style={styles.loginContainer}>
  { dialogError() }
  <Paper style={styles.paper}>
    <form onSubmit = {action}>
      <TextField
        hintText="E-mail"
        name="email"
        errorText = {errors && errors.email}
        floatingLabelText="E-mail"
        onFocus = {onChangeField}
        fullWidth={true}
      />
      <TextField
        hintText="Password"
        name="password"
        floatingLabelText="Password"
        errorText = {errors && errors.password}
        fullWidth={true}
        onFocus = {onChangeField}
        type="password"
      />
      <div>
        <Checkbox
          label="Remember me"
          style={styles.checkRemember.style}
          labelStyle={styles.checkRemember.labelStyle}
          iconStyle={styles.checkRemember.iconStyle}
        />
        <RaisedButton type = "submit" label="Login"
                      primary={true}
                      style={styles.loginBtn} />
         </div>
    </form>
  </Paper>
  <LoginButtons />
 </div>
);

};

LoginForm.propTypes = {
  action: PropTypes.func,
  error: PropTypes.object
};

export default LoginForm;       