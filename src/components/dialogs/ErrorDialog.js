import React, { PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

export default class ErrorDialog extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: props.open ? props.open : false,
      message: props.message
    };

    this.handleClose = this.handleClose.bind(this);
  }

  handleClose(){
    this.setState({open: false});
    this.props.closeDialog({
      errorFound: false,
      processFailedMessage: ''
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({message : nextProps.message});
    this.setState({open : nextProps.open});
  }

  render() {
    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.handleClose}
      />
    ];
    return (
      <div>
        <Dialog
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          {this.state.message}
        </Dialog>
      </div>
    );
  }
}

ErrorDialog.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
};
