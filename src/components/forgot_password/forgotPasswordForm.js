import React, {PropTypes} from 'react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import {grey500} from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';
import ForgotPasswordFooterButton from './forgotPasswordFooterButton';
// import {Link} from 'react-router';

const ForgotPasswordForm = (props) => {

const {
  action,
  errors,
  onChangeField,
  loginFailedMessage,
  loginFailed,
  closeDialog
} = props;

const styles = {
  forgotPasswordContainer: {
      minWidth: 320,
      maxWidth: 400,
      height: 'auto',
      position: 'absolute',
      top: '20%',
      left: 0,
      right: 0,
      margin: 'auto'
  },
  paper: {
    padding: 20,
    overflow: 'auto'
  },
  checkRemember: {
    style: {
      float: 'left',
      maxWidth: 180,
      paddingTop: 5
    },
    labelStyle: {
      color: grey500
    },
    iconStyle: {
      color: grey500,
      borderColor: grey500,
      fill: grey500
    }
  },
  forgotPasswordBtn: {
    float: 'right'
  },
};


return(
 <div style={styles.forgotPasswordContainer}>
  <Paper style={styles.paper}>
    <form onSubmit = {action}>
      <TextField
        hintText="Password"
        name="password"
        floatingLabelText="Enter New Password"
        errorText = {errors && errors.password}
        fullWidth={true}
        onFocus = {onChangeField}
        type="password"
      />
      <TextField
        hintText="Password"
        name="c_password"
        floatingLabelText="Confirm New Password"
        errorText = {errors && errors.c_password}
        fullWidth={true}
        onFocus = {onChangeField}
        type="password"
      />
      <div>
        <RaisedButton type = "submit" label="Submit"
                      primary={true}
                      style={styles.forgotPasswordBtn} />
         </div>
    </form>
  </Paper>
  <ForgotPasswordFooterButton />
 </div>
);

};


export default ForgotPasswordForm;       