import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import AccountBox from 'material-ui/svg-icons/action/account-box';
import {grey500} from 'material-ui/styles/colors';
import { Link } from 'react-router';

const ForgotPasswordFooterButton = () => {

const styles = {
  buttonsDiv: {
    textAlign: 'center',
    padding: 10
  },
  flatButton: {
    color: grey500
  },
};

return(
  <div style={styles.buttonsDiv}>
    <Link to="/login">
      <FlatButton
        label="Login"
        style={styles.flatButton}
        icon={<AccountBox />}
      />
    </Link>
  </div>
);
};


export default ForgotPasswordFooterButton;