import React from 'react';
import { Marker, Popup } from 'react-leaflet';



class Parkers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      parkings: props.parkings,
      increment: props.increment ? props.increment: true,
    };
    
  }

  render() {
   
    const ParkersArray = this.state.parkings.map((value,index) => {
      console.log(value);
      return (
        <Marker key={index} position={[value.position.lat,value.position.lng]} >
          <Popup >
            <div className="row">
              <span style={{marginLeft: 15}}>Service Zone: {"Zone 1"}</span><br />
              <span style={{marginLeft: 15}}>Name: {value.name}</span><br />
              <div className="col-md-6 col-xs-6 col-sm-6">
                  <p><b>Operating Times:</b></p>
                  <ul style={{padding: 15,
    paddingTop: 0,paddingBottom:0}}>
                    <li>opening ({value.time.opening})</li>
                    <li>closing ({value.time.closing})</li>
                  </ul><br />
              </div>
              
              <div className="col-md-6 col-xs-6 col-sm-6">
              <p><b>Services:</b></p>
              <ul style={{padding: 15,
    paddingTop: 0,paddingBottom:0}}>
                  {
                    value.services.map((v,i) => {
                      return <li>{v.title} ({v.status})</li>
                    })
                  }
              </ul><br />
              </div>
              <div className="col-md-12 col-xs-12 col-sm-12" style={{marginTop:-35}}>
              <p><b>Space Available:</b></p>
              <ul style={{padding: 15,
    paddingTop: 0,paddingBottom:0}}>
                <li>Total Parking Space: ({value.parkingSpace})</li>
                <li>Total Cars Parked: ({value.totalCarsParked})</li>
              </ul><br />
              <span>Status: {"Available"}</span>
            </div>
            </div>
          </Popup>
        </Marker>
        );
    });
    console.log("Parkers rendering again");
    return (
    <div>
       {ParkersArray}
    </div>
    );
  }
};

export default Parkers;
