import React, {PropTypes} from 'react';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {grey400, white} from 'material-ui/styles/colors';
import {typography} from 'material-ui/styles';
import GlobalStyles from "../../styles";
import Wallpaper from 'material-ui/svg-icons/device/wallpaper';
import Face from 'material-ui/svg-icons/action/face';

const BookingsLiveFeed = (props) => {

  const styles = {
    subheader: {
      fontSize: 24,
      fontWeight: typography.fontWeightLight,
      backgroundColor: GlobalStyles.appColors.grey,
      color: white
    },
    listing:{
      height: 290,
      overflowY: "scroll",
    }
  };

  const iconButtonElement = (
    <IconButton
      touch={true}
      tooltipPosition="bottom-left"
    >
      <MoreVertIcon color={grey400} />
    </IconButton>
  );

  const rightIconMenu = (
    <IconMenu iconButtonElement={iconButtonElement}>
      <MenuItem>View</MenuItem>
    </IconMenu>
  );

  return (
    <Paper>
      <List>
        <Subheader style={styles.subheader}>Bookings Live Feed</Subheader>
        <div style = {styles.listing} >
        {props.data.map(item => {
          const Services = "Service: Zone 1, Valet: Valet 1";
          const rating = Services+" , Time: 24/04/2017";
          return(
          <div key={item.title}>
            <ListItem
            innerDivStyle={{fontSize:20,margin:5}}
              leftAvatar={<Avatar src="https://s-media-cache-ak0.pinimg.com/736x/af/5a/ac/af5aac155845e889523d038e29da8684.jpg" />}
              primaryText={"New Booking Request (20109920)"}
              secondaryText={<div style={{fontSize:15,marginTop:10}}>{rating}</div>}
              rightIconButton={rightIconMenu}
            />
            <Divider inset={true} />
          </div>
          );
        }
        )}
        </div>
      </List>
    </Paper>
  );
};

BookingsLiveFeed.propTypes = {
  data: PropTypes.array
};

export default BookingsLiveFeed;
