import React from 'react';
import { Circle, Popup } from 'react-leaflet';

class Zones extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const Zones = this.props.ServiceZone.map((item,index) => {
      const centerPosition = [item.areaPolygons.lat,item.areaPolygons.lng];
      return (
        <Circle color={item.color} center={centerPosition} radius= {item.areaPolygons.radius} >
          <Popup>
            <span>This is {item.name} </span>
          </Popup>
        </Circle>
        );
    });
    console.log("markers rendering again");
    return (
        <div>
          {Zones}
        </div>
    );
  }
};

export default Zones;
