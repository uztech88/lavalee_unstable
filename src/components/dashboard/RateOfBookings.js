import {PieChart, Pie, Sector, ResponsiveContainer} from 'recharts';
import React from 'react';
import Paper from 'material-ui/Paper';
import GlobalStyles from '../../styles';

                   
const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
    fill, payload, percent, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`PV ${value}`}</text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(Rate ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

class RateOfBookings extends React.Component{
	constructor() {
    super();
    this.state= {
      activeIndex: 0,
    
    };
    this.onPieEnter = this.onPieEnter.bind(this);
  }

  onPieEnter(data, index) {
    this.setState({
      activeIndex: index,
    });
  }


	render () {

    const styles = {
    paper: {
      backgroundColor: "#FFFFFF",
      height: 360
    },
    div: {
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '95%',
      height: 300
    },
    header: {
      color: "#FFFFFF",
      backgroundColor: GlobalStyles.appColors.grey,
      padding: 10
    }
  };

    const data = [{name: 'Zone A', value: 400}, {name: 'Zone B', value: 300},
                  {name: 'Zone C', value: 300}, {name: 'Zone D', value: 200}];
  	return (
    <Paper style={styles.paper}>
      <div style={{...GlobalStyles.title, ...styles.header}}>Rate of Bookings in Zones</div>
      <div style={styles.div}>
        <ResponsiveContainer>
    	<PieChart width={800} height={300} onMouseEnter={this.onPieEnter}>
        <Pie 
        isAnimationActive={false}
        	activeIndex={this.state.activeIndex}
          activeShape={renderActiveShape} 
          data={data} 
          cx={300} 
          cy={135} 
          innerRadius={80}
          outerRadius={110} 
          fill="#8884d8"/>
       </PieChart>
       </ResponsiveContainer>
       </div>
    </Paper>
    );
  }
}

export default RateOfBookings;