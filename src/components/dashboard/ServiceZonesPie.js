import React, { PropTypes } from 'react';
import Paper from 'material-ui/Paper';
import {PieChart, Pie, Cell, ResponsiveContainer, Tooltip} from 'recharts';
import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import GlobalStyles from '../../styles';
import Subheader from 'material-ui/Subheader';
import {typography} from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

const ServiceZonesPie = (props) => {

  const styles = {
    subheader: {
      fontSize: 24,
      fontWeight: typography.fontWeightLight,
      backgroundColor: GlobalStyles.appColors.grey,
      color: "#FFFFFF"
    },
    paper: {
      minHeight: 344,
    },
    legend: {
      paddingTop: 20,
    },
    pieChartDiv: {
      height: 290,
      textAlign: 'center'
    }
  };

  const zones = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton iconStyle = {{color:"white"}} ><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Zone 1" checked={true} />
    <MenuItem primaryText="Zone 2" checked={true}/>
    <MenuItem primaryText="Zone 3" checked={true} />
    <MenuItem primaryText="Zone 4" />
    <MenuItem primaryText="Zone 5" />
  </IconMenu>
  );

  return (
    <Paper style={styles.paper}>
    <AppBar
          title="No of Parking Lots"
          style = {{zIndex: 0,backgroundColor: GlobalStyles.appColors.grey}}
          iconElementRight={zones()}
          iconElementLeft={<span></span>}
        />
      <div style={GlobalStyles.clear}/>

      <div className="row">

        <div className="col-xs-12 col-sm-8 col-md-8 col-lg-8">
          <div style={styles.pieChartDiv}>
            <ResponsiveContainer>
              <PieChart >
              <Pie
                 data={props.data}  
                 cy={150} 
                 outerRadius={100} 
                 fill="#8884d8" label>
                 {
                    props.data.map((item) => <Cell key={item.name} fill={item.color}/>)
                 }
                </Pie>
                <Tooltip />
              </PieChart>
            </ResponsiveContainer>
          </div>
        </div>

        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <div style={styles.legend}>
            <div style={styles.legend}>
              <List>
                {props.data.map((item) =>
                  <ListItem
                    key={item.name}
                    leftAvatar={
                      <Avatar children = {<span>{item.value}</span>}
                              backgroundColor={item.color}/>
                    }>
                    {item.name}
                  </ListItem>
                )}
              </List>
            </div>
          </div>
        </div>
      </div>
    </Paper>
  );
};

ServiceZonesPie.propTypes = {
  data: PropTypes.array
};

export default ServiceZonesPie;
