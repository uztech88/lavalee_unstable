import React, { PropTypes } from 'react';
import Paper from 'material-ui/Paper';
import {BarChart, Bar, XAxis, YAxis,
  CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import GlobalStyles from '../../styles';
import Subheader from 'material-ui/Subheader';
import {typography} from 'material-ui/styles';
import Data from '../../data';
import AppBar from 'material-ui/AppBar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

const ServiceZonesActivityCompare = (props) => {

  const styles = {
    subheader: {
      fontSize: 24,
      marginBottom: 20,
      fontWeight: typography.fontWeightLight,
      backgroundColor: GlobalStyles.appColors.grey,
      color: "#FFFFFF"
    },
    paper: {
      minHeight: 344,
      marginBottom: 15,
    },
    legend: {
      paddingTop: 20,
    },
    pieChartDiv: {
      height: 290,
      textAlign: 'center'
    }
  };

   const zones = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton iconStyle = {{color:"white"}} ><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Zone 1" checked={true} />
    <MenuItem primaryText="Zone 2" checked={true}/>
    <MenuItem primaryText="Zone 3" checked={true} />
    <MenuItem primaryText="Zone 4" />
    <MenuItem primaryText="Zone 5" />
  </IconMenu>
  );

  return (
    <Paper style={styles.paper}>
       <AppBar
          title="No of Parking Lots"
          style = {{zIndex: 0,backgroundColor: GlobalStyles.appColors.grey}}
          iconElementRight={zones()}
          iconElementLeft={<span></span>}
        />
      <div style={GlobalStyles.clear}/>
      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div style={styles.pieChartDiv}>
            <ResponsiveContainer>
              <BarChart width={600} height={300} data={props.data}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
               <XAxis dataKey="name"/>
               <YAxis/>
               <CartesianGrid strokeDasharray="3 3"/>
               <Tooltip/>
               <Legend iconType="circle" />
               {
                Data.ZonesColors.map((value,index) => {
                  const zname = "Zone"+(index+1);
                  return <Bar dataKey={zname} fill={value} />;
                })
                } 
              </BarChart>
            </ResponsiveContainer>
          </div>
        </div> 
      </div>
    </Paper>
  );
};

ServiceZonesActivityCompare.propTypes = {
  data: PropTypes.array
};

export default ServiceZonesActivityCompare;
