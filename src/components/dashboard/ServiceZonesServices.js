import React, {PropTypes} from 'react';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {grey400, white} from 'material-ui/styles/colors';
import {typography} from 'material-ui/styles';
import GlobalStyles from "../../styles";
import trackChanges from 'material-ui/svg-icons/action/track-changes';
import Chip from 'material-ui/Chip';
import Toggle from 'material-ui/Toggle';
import AppBar from 'material-ui/AppBar';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import Clear from 'material-ui/svg-icons/content/clear';
import Done from 'material-ui/svg-icons/action/done';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';


const ServiceZonesServices = (props) => {

  const styles = {
    subheader: {
      fontSize: 24,
    },
    ZoneHeader: {
      fontSize: 25,
    },
    circle:{
      height: 100,
      width: 100,
      margin: 20,
      textAlign: 'center',
      display: 'inline-block',
    },
    chip:{
      margin:4,
      backgroundColor:"rgb(47, 188, 192)",
      fontColor:"white",
    },
    chipInActive:{
       margin:4,
      backgroundColor:"rgb(244, 67, 54)",
      fontColor:"white",
      color:"white",
    }
  };

  const zones = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton iconStyle = {{color:"white"}} ><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Zone 1" checked={true} />
    <MenuItem primaryText="Zone 2" />
    <MenuItem primaryText="Zone 3" />
  </IconMenu>
  );

  const iconButtonElement = (
    <IconButton
      touch={true}
      tooltipPosition="bottom-left"
    >
      <MoreVertIcon color={grey400} />
    </IconButton>
  );

  const rightIconMenu = (
    <IconMenu iconButtonElement={iconButtonElement}>
      <MenuItem>View</MenuItem>
    </IconMenu>
  );

  const childrens = (item) => {
    return item.services.map((v,i) => {
                  return <ListItem primaryText={v.title} rightToggle={<Toggle defaultToggled={true} />} />
            });
  };

  const innerCards = (color,title,value) => {
    return (
       <Card style = {{paddingBottom:15,
        boxShadow: "5px 5px 5px #888888",
        borderLeft:"4px solid",borderTop:"4px solid",
        borderRadius:25,
        borderColor:color,margin:18}}>
          <CardTitle style={{textAlign: "center"}}>
          {title}
          </CardTitle>
           <CardMedia style = {{fontSize: 30,textAlign:"center",color:GlobalStyles.appColors.grey}} >
            <div className="count">{value}</div>
           </CardMedia>
        </Card>
      );
  }

  const parkinglot = () => {
    return (
      <div>
      <Subheader>Parking Space</Subheader>
      <div className = "row">
        <div className= "col-lg-4 col-md-4 col-sm-4">
          {innerCards("red","Total Parked",10)}
        </div>
        <div className= "col-lg-4 col-md-4 col-sm-4">
         {innerCards("purple","Spaces Left",4)}
        </div>
        <div className= "col-lg-4 col-md-4 col-sm-4">
        {innerCards("orange","Upcoming",3)}
        </div>
      </div>
      <Subheader>Services</Subheader>
        <div className="row" style={{paddingLeft:30}}>
        <Chip
          style={styles.chip}>
          <Avatar style ={{backgroundColor:"transparent"}} icon={<Done />} />
          Car Washing
        </Chip>
        <Chip
          style={styles.chip}>
           <Avatar style ={{backgroundColor:"transparent"}} icon={<Done />} />
          Fuel Refilling
        </Chip>
         <Chip
          style={styles.chipInActive}>
           <Avatar style ={{backgroundColor:"transparent"}} icon={<Clear />} />
          Car Repairing
        </Chip>
        </div>
        
        <div className = "row">
        <div className= "col-lg-4 col-md-4 col-sm-4">
          {innerCards("red","In Service",3)}
        </div>
        <div className= "col-lg-4 col-md-4 col-sm-4">
         {innerCards("purple","Pending Service",2)}
        </div>
        <div className= "col-lg-4 col-md-4 col-sm-4">
        {innerCards("orange","Completed",1)}
        </div>
      </div>
      </div>
      );
  };

  return (
    <Paper>
      <AppBar
          title="Services in Parking Lots"
          style = {{zIndex: 0,backgroundColor: GlobalStyles.appColors.grey}}
          iconElementRight={zones()}
          iconElementLeft={<span></span>}
        />
      <List>
          <div>
             <List>
              <Subheader style= {styles.subheader}>Zone 1</Subheader>
              <ListItem
              primaryText="Parking Lot 1"
              leftIcon={<ContentInbox />}
              primaryTogglesNestedList={true}
              nestedItems={[
                <ListItem innerDivStyle={{padding:0,margin:0}} children={parkinglot()} />,
                ]}
              />
              <ListItem
              primaryText="Parking Lot 2"
              leftIcon={<ContentInbox />}
              primaryTogglesNestedList={true}
              nestedItems={[
                <ListItem innerDivStyle={{padding:0,margin:0}} children={parkinglot()} />,
                ]}
              />
              </List>
          </div>
      </List>
    </Paper>
  );
};

ServiceZonesServices.propTypes = {
  data: PropTypes.array
};

export default ServiceZonesServices;
