import React from 'react';
import { Map, TileLayer, Circle, Popup } from 'react-leaflet';
import Markers from './Markers';
import Parkings from './Parkings';
import Zones from './Zones';
import Paper from 'material-ui/Paper';
import Subheader from 'material-ui/Subheader';
import {typography} from 'material-ui/styles';
import GlobalStyles from "../../styles";

class MapComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ServiceZone: this.props.ServiceZone[0],
      heading: this.props.heading,
      zoom: 10,
    };
  }


  render() {
    const styles = {
    subheader: {
      fontSize: 24,
      fontWeight: typography.fontWeightLight,
      backgroundColor: GlobalStyles.appColors.grey,
      color: "#FFFFFF"
    },
  };
    const centerPosition = [this.state.ServiceZone.areaPolygons.lat,
    this.state.ServiceZone.areaPolygons.lng];
    console.log("map rendering again");
     const markers = () => {
      if(this.props.parking){
        return <Parkings parkings = {this.props.ServiceZone[0].parkingLots} />
      }else{
        return <Markers Valets = {this.props.ServiceZone[0].valets} />
      }
    };
    return (
      <Paper>
      <Subheader style={styles.subheader} >{this.state.heading}</Subheader>
      <Map center={centerPosition} zoom={this.state.zoom}>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />

       <Zones ServiceZone={this.props.ServiceZone} />
       {markers()}
      </Map>
      </Paper>
    );
  }
};

export default MapComponent;
