import React, { PropTypes } from 'react';
import Paper from 'material-ui/Paper';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import "../../css/own.css";
import {BarChart, Bar,LineChart, Line, ComposedChart ,AreaChart, Area, ResponsiveContainer} from 'recharts';

const BookingTopSection = () => {
	const styles = {
		row: {
			flexWrap: "nowrap", 
		},
		cardText: {
			padding: 2,
			backgroundColor: "rgba(0,0,0,0.3)"
		},
		greenWhiteCard: {
			backgroundColor: "#4CAF50",
		},
		cardMedia: {
			padding: 30,
			color: "#FFFFFF",
		},
	
		blueGreyCard: {
			backgroundColor: "#607d8b",
		},
		pinkLightenCard: {
			backgroundColor: "#ec407a",
		},
		purpleCard: {
			backgroundColor: "#9c27b0",
		}
	}

	const data = [
      {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
      {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
      {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
      {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
      {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
      {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
      {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page Z', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page K', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page N', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page M', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page z', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page 1', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page 2', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page xa', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page 1d', uv: 3490, pv: 4300, amt: 2100},
      {name: 'Page 22', uv: 3490, pv: 4300, amt: 2100},
];
	return (
		  <div style = {styles.row} className="row center-lg center-md center-sm tile_count">
        <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6 tile_stats_count">
        	<div className = "box">
        		<Card style = {styles.greenWhiteCard}>
        		 <CardMedia style = {styles.cardMedia} >
        		 <span className="count_top"><i className="fa fa-user"></i> New Clients</span>
	          <div className="count">25</div>
	          <span className="count_bottom"><i className="fa fa-sort-asc"></i><i>2% </i> From last Week</span>
				    </CardMedia>
				    <CardActions style = {styles.cardText}>
				    <ResponsiveContainer width ="100%" height={40} >
				    	<BarChart data={data}>
				         <Bar dataKey='uv' fill='#FFFFFF'/>
				       </BarChart>
				     </ResponsiveContainer>
				    </CardActions>
				    </Card>
          </div>
        </div>
        <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6 tile_stats_count">
          <div className = "box">
         <Card style = {styles.blueGreyCard}>
        		 <CardMedia style = {styles.cardMedia} >
        		 <span className="count_top"><i className="fa fa-user"></i> Total Bookings</span>
	          <div className="count">200</div>
	          <span className="count_bottom"><i className="fa fa-sort-asc"></i><i >3% </i> From Yesterday</span>
				    </CardMedia>
				    <CardActions style = {styles.cardText}>
				    <ResponsiveContainer width ="100%" height={40} >
				    	<LineChart data={data}>
				        <Line type='monotone' dataKey='pv' stroke='#FFFFFF' strokeWidth={2} />
				      </LineChart>
				     </ResponsiveContainer>
				    </CardActions>
				    </Card>
          </div>
        </div>
        <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6 tile_stats_count">
          <div className = "box">
         <Card style = {styles.purpleCard}>
        		<CardMedia style = {styles.cardMedia} >

	        		 <span className="count_top"><i className="fa fa-user"></i> Canceled Bookings </span>
	              <div className="count ">10</div>
	              <span className="count_bottom"><i className="fa fa-sort-desc"></i>12% From Yesterday</span>
				   
				    </CardMedia>
				    <CardActions style = {styles.cardText}>
				    
				    <ResponsiveContainer width ="100%" height={40} >
				    	<ComposedChart data={data}>
								  <Area type="monotone" dataKey="amt" fill="#FFFFFF" stroke="#FFFFFF" />
								  <Bar dataKey="pv" barSize={20} fill="#FFFFFF" />
								  <Line type="monotone" dataKey="uv" stroke="#000000" />
							</ComposedChart>
				     </ResponsiveContainer>
				    
				    </CardActions>
				    </Card>
          </div>
        </div>
        <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6 tile_stats_count">
          <div className = "box">
         <Card style = {styles.pinkLightenCard}>
        		 <CardMedia style = {styles.cardMedia} >

	        	<span className="count_top"><i className="fa fa-user"></i> Upcoming Bookings</span>
	          <div className="count">20</div>
	          <span className="count_bottom"><i className="fa fa-sort-asc"></i><i>4% </i> From Yesterday</span>
				 
				    </CardMedia>
				    <CardActions style = {styles.cardText}>
				    
				    <ResponsiveContainer width ="100%" height={40} >

				    	<AreaChart data={data} syncId="anyId">
			          <Area type='monotone' dataKey='pv' stroke='#FFFFFF' fill='#FFFFFF' />
			        </AreaChart>

				     </ResponsiveContainer>
				    
				    </CardActions>
				    </Card>
          </div>
        </div>
     </div>
		);
};

export default BookingTopSection;
