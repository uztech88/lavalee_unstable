import React, {PropTypes} from 'react';
import Paper from 'material-ui/Paper';
import {white, pink600, pink500} from 'material-ui/styles/colors';
import {BarChart, Bar, ResponsiveContainer, XAxis, YAxis, Tooltip} from 'recharts';
import GlobalStyles from '../../styles';

const MonthlySales = (props) => {

  const styles = {
    paper: {
      backgroundColor: GlobalStyles.appColors.cyan,
      height: 300
    },
    div: {
      marginRight: 'auto',
      width: '95%',
      height: 200
    },
    header: {
      color: white,
      backgroundColor: GlobalStyles.appColors.grey,
      padding: 10
    }
  };

  return (
    <Paper style={styles.paper}>
      <div style={{...GlobalStyles.title, ...styles.header}}>Total Bookings By Month</div>
      <div style={styles.div}>
        <ResponsiveContainer>
          <BarChart data={props.data} >
            <Bar dataKey="bookings" fill={GlobalStyles.appColors.grey}/>
            <XAxis dataKey="name" stroke="none" tick={{fill: white}}/>
            <YAxis />
             <Tooltip/>
          </BarChart>
        </ResponsiveContainer>
      </div>
    </Paper>
  );
};

MonthlySales.propTypes = {
  data: PropTypes.array
};

export default MonthlySales;
