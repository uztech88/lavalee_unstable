import React from 'react';
import { Marker, Popup } from 'react-leaflet';
import Avatar from 'material-ui/Avatar';


class Markers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      valets: props.Valets,
      increment: props.increment ? props.increment: true,
    };
    this.incrementPositions = this.incrementPositions.bind(this);
  }

  incrementPositions(){
    setTimeout(() => {
        let valet = this.state.valets.map((value,index) => {
          value.position.lat += 0.001;
          value.position.lng += 0.001;
          return value;
        });
        this.setState({valets: valet});
    },2000);
  }

  render() {
   
    const MarkersArray = this.state.valets.map((value,index) => {
      return (
        <Marker key={index} position={[value.position.lat,value.position.lng]} >
          <Popup >
            <div style={{width: 100,textAlign:"center"}}>
              <img style={{width:50,height:50, borderRadius:20}} src="https://s-media-cache-ak0.pinimg.com/736x/af/5a/ac/af5aac155845e889523d038e29da8684.jpg" />
              <br /><span>Name: {value.name}</span><br />
              <span>Rating: {value.rating}</span><br />
              <span>Status: {value.status}</span>
            </div>
          </Popup>
        </Marker>
        );
    });
    console.log("markers rendering again");
     {this.state.increment ? this.incrementPositions(): null}
    return (
    <div>
       {MarkersArray}
    </div>
    );
  }
};

export default Markers;
