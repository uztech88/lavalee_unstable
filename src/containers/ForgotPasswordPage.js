import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { connect } from 'react-redux';
import ThemeDefault from '../theme-default';
import ForgotPasswordForm from '../components/forgot_password/forgotPasswordForm';

class ForgotPasswordPage extends React.Component{

  render(){
    console.log("runing forgot_password page render");
    return (
      <MuiThemeProvider muiTheme={ThemeDefault}>
        <ForgotPasswordForm />
      </MuiThemeProvider>
    );
  }
}

export default connect()(ForgotPasswordPage);
