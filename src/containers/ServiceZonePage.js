import React from 'react';
import {green400, pink600, purple600, orange600} from 'material-ui/styles/colors';
import Shuttle from 'material-ui/svg-icons/places/airport-shuttle';
import Asignment from 'material-ui/svg-icons/action/assignment-turned-in';
import Backspace from 'material-ui/svg-icons/content/backspace';
import Place from 'material-ui/svg-icons/maps/place';
import InfoBox from '../components/dashboard/InfoBox';
import NewOrders from '../components/dashboard/NewOrders';
import MonthlySales from '../components/dashboard/MonthlySales';
import ServiceZonesPie from '../components/dashboard/ServiceZonesPie';
import ServiceZonesActivityCompare from '../components/dashboard/ServiceZonesActivityCompare';
import RecentlyProducts from '../components/dashboard/RecentlyProducts';
import MapComponent from '../components/dashboard/MapComponent';
import globalStyles from '../styles';
import Data from '../data';
import ServiceZonesServices from '../components/dashboard/ServiceZonesServices';

class ServiceZonePage extends React.Component {

  constructor(props){
  super(props);
  this.state = {
    ServiceZone: Data.CoreData.ServiceZones,
    activeServiceZones:2,
  }
}  

render(){
  return (
    <div>
      <div className="row">

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Place}
                   color={green400}
                   style={{fontSize:20}}
                   title="Total Service Zones"
                   value={this.state.ServiceZone.length}
          />
        </div>


        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Backspace}
                   color={pink600}
                   title="Total InActive Zones"
                   value="0"
          />
        </div>

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Shuttle}
                   color={purple600}
                   title="Total Parking Lots"
                   value="10"
          />
        </div>

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Asignment}
                   color={orange600}
                   title="Active Parking Lots"
                   value="9"
          />
        </div>
      </div>

      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <ServiceZonesActivityCompare data={Data.ZonesComparisions} />
        </div>
      </div>

       <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <MapComponent ServiceZone = {Data.CoreData.ServiceZones} parking={true} heading={"Parking Lots Locations"} />
        </div>
      </div>

      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
          <ServiceZonesServices data={Data.dashBoardPage.recentProducts} />
        </div>

        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
          <ServiceZonesPie data={Data.dashBoardPage.ParkingLotsInZones}/>
        </div>
      </div>

    </div>
  );
}
};

export default ServiceZonePage;
