import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { connect } from 'react-redux';
import ThemeDefault from '../theme-default';
import RecoveryMailForm from '../components/recovery_mail/recoveryMailForm';

const RecoveryMailPage = () => {
  return (
  	<MuiThemeProvider muiTheme={ThemeDefault}>
      <RecoveryMailForm />
    </MuiThemeProvider>
    );
}

export default connect()(RecoveryMailPage);
