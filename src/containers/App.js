import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Header from '../components/Header';
import {connect} from 'react-redux';
import LeftDrawer from '../components/LeftDrawer';
import withWidth, {LARGE, SMALL} from 'material-ui/utils/withWidth';
import ThemeDefault from '../theme-default';
import Data from '../data';
import { submitLogout } from '../actions/authActions';
import { browserHistory } from 'react-router';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      navDrawerOpen: false
    };

    this.logout = this.logout.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.width !== nextProps.width) {
      this.setState({navDrawerOpen: nextProps.width === LARGE});
    }
    if (!nextProps.isLoggedIn){
        browserHistory.push("/login");
    }
  }

  handleChangeRequestNavDrawer() {
    this.setState({
      navDrawerOpen: !this.state.navDrawerOpen
    });
  }

  // dialogError() {
  //   const { errorFound , processFailedMessage,
  //    closeDialog } = this.props;

  //   if(errorFound){
  //     console.log("logoutErrorDialog hit");
  //     return <ErrorDialog open = {errorFound}
  //     message = {processFailedMessage} closeDialog = {closeDialog} />;
  //   }
  // };

  logout(){
    this.props.logout(this.props.userDetails.user.token);
  }

  render() {
    let { navDrawerOpen } = this.state;
    const paddingLeftDrawerOpen = 236;

    const styles = {
      header: {
        paddingLeft: navDrawerOpen ? paddingLeftDrawerOpen : 0
      },
      container: {
        margin: '80px 20px 20px 15px',
        paddingLeft: navDrawerOpen && this.props.width !== SMALL ? paddingLeftDrawerOpen : 0
      }
    };

    return (
      <MuiThemeProvider muiTheme={ThemeDefault}>
        <div>
          <Header styles={styles.header}
                  handleChangeRequestNavDrawer={
                    this.handleChangeRequestNavDrawer.bind(this)
                  }
                  logout={this.logout}/>

          <LeftDrawer navDrawerOpen={navDrawerOpen}
                      menus={Data.menus}
                      username={Data.user.username}/>

          <div style={styles.container}>
            {this.props.children}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state,OwnProps) => { 
    console.log(state);
    return {
      userDetails: state.userReducer,
      isLoggedIn: state.loginReducer.isLoggedIn,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
      logout: token => dispatch(submitLogout(token)),
    };
};

App.propTypes = {
  children: PropTypes.element,
  width: PropTypes.number
};

export default connect(mapStateToProps,mapDispatchToProps)(App);
