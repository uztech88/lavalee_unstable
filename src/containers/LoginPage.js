import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {connect} from 'react-redux';
import ThemeDefault from '../theme-default';
import { submitFromB, loginIncorrect, loginValidationFailed, 
  removeFieldError, submitLogin } from '../actions/authActions';
import { dialogClose } from '../actions/dialogActions';
import LoginForm from '../components/login/loginForm';
import checkLoginValidation from '../utils/loginValidation';
import { browserHistory } from 'react-router';

class LoginPage extends React.Component{
  constructor(props){
    super(props);
  
    this.submitForm = this.submitForm.bind(this);
    this.onChangeField = this.onChangeField.bind(this);
  }

  onChangeField(field){
    const error = this.props.errors;
    const name = field.target.name;
    if(error && error[name]){
     this.props.removeFieldError(name);
    };
  };

  componentWillReceiveProps(nextProps) {
    if(nextProps.isLoggedIn){
      browserHistory.push("/op_panel");
    }
  }
  
  submitForm(form){
    form.preventDefault();
    let res = checkLoginValidation(form);
    if(res.isError){
      this.props.loginValidationFailed(res.errors);
    }else{
      console.log("api function fired");
      this.props.submitLogin(form);
    }
  }

  render(){
    console.log("runing login page render");
    return (
      <MuiThemeProvider muiTheme={ThemeDefault}>
        <LoginForm action={this.submitForm} errors = {this.props.errors}
        onChangeField={this.onChangeField}
        closeDialog={this.props.closeDialog} 
        loginFailedMessage={this.props.loginFailedMessage}
        loginFailed={this.props.loginFailed} />
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state,OwnProps) => {
  const { loginReducer,
    redirectionReducer } = state;
  return {
    errors: loginReducer.errors,
    loginFailed: loginReducer.isError,
    loginFailedMessage: loginReducer.errorMessage,
    isLoggedIn: loginReducer.isLoggedIn,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginIncorrect: error => dispatch(loginIncorrect(error)),
    loginValidationFailed: errors => dispatch(loginValidationFailed(errors)),
    removeFieldError: name => dispatch(removeFieldError(name)),
    submitLogin: form => dispatch(submitLogin(form)),
    closeDialog: fields => dispatch(dialogClose(fields))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
