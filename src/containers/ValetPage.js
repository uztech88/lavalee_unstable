import React from 'react';
import {cyan600, pink600, purple600, orange600} from 'material-ui/styles/colors';
import Assessment from 'material-ui/svg-icons/action/assessment';
import Face from 'material-ui/svg-icons/action/face';
import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
import ShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import InfoBox from '../components/dashboard/InfoBox';
import NewOrders from '../components/dashboard/NewOrders';
import MonthlySales from '../components/dashboard/MonthlySales';
import BrowserUsage from '../components/dashboard/BrowserUsage';
import RecentlyProducts from '../components/dashboard/RecentlyProducts';
import MapComponent from '../components/dashboard/MapComponent';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import globalStyles from '../styles';
import Data from '../data';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

class ValetPage extends React.Component {

constructor(props){
  super(props);
  this.state = {
    ServiceZone: Data.CoreData.ServiceZones,
  }
}  

render(){
  console.log(this.state.ServiceZone);
  const ServiceZonesMenu = this.state.ServiceZone.map((value,index) => {
    return (<MenuItem value={index} key={index} primaryText={value.name} />);
  });

  const innerCards = (color,title,value) => {
    return (
       <Card style = {{paddingBottom:15,
        boxShadow: "5px 5px 5px #888888",
        borderLeft:"6px solid",
        borderRadius: 10,
        borderColor:color,margin:18}}>
          <CardTitle style={{fontSize:20,textAlign: "center"}}>
          {title}
          </CardTitle>
           <CardMedia style = {{fontSize: 35,textAlign:"center",color:globalStyles.appColors.grey}} >
            <div className="count" style= {{paddingBottom: 15}}>
            <div><i className="fa fa-user" style={{marginBottom:10}}></i></div>
            {value}</div>
           </CardMedia>
        </Card>
      );
  }

  return (
    <div>
      <div className="row">
        <div className ="col-lg-3 col-md-3" >
          {innerCards("rgb(236, 64, 122)","Total Valets","350")}
        </div>
        <div className ="col-lg-3 col-md-3" >
          {innerCards("orange","Total Available Valets","60")}
        </div>
        <div className ="col-lg-3 col-md-3" >
          {innerCards("black","Total Busy Valets","100")}
        </div>
        <div className ="col-lg-3 col-md-3" >
          {innerCards("rgb(47, 188, 192)","Total Offline Valets","10")}
        </div>
      </div>
      <div className="row">
        <div className ="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3" >
          <SelectField
          floatingLabelText="Select Service Zone"
          floatingLabelStyle={{color:"black",fontSize: 20}}
          labelStyle={{color:globalStyles.appColors.cyan}}
          iconStyle={{fill:"black"}}
          underlineStyle={{borderColor:globalStyles.appColors.grey}}
          fullWidth= {true}
          value={0}
        >
          {ServiceZonesMenu}
        </SelectField>
        </div>
      </div>

      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12">
          <MapComponent  ServiceZone = {this.state.ServiceZone} heading={"Valets Live Locations"} />
        </div>
      </div>

      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
          <RecentlyProducts data={Data.dashBoardPage.TopValets}/>
        </div>

        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
          <BrowserUsage data = {Data.dashBoardPage.ValetsStatus} />
        </div>
      </div>
    </div>
  );
}
};

export default ValetPage;
