import React from 'react';
import {cyan600, pink600, purple600, orange600} from 'material-ui/styles/colors';
import Assessment from 'material-ui/svg-icons/action/assessment';
import Face from 'material-ui/svg-icons/action/face';
import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
import ShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import InfoBox from '../components/dashboard/InfoBox';
import NewOrders from '../components/dashboard/NewOrders';
import MonthlySales from '../components/dashboard/MonthlySales';
import BrowserUsage from '../components/dashboard/BrowserUsage';
import RecentlyProducts from '../components/dashboard/RecentlyProducts';
import MapComponent from '../components/dashboard/MapComponent';
import RateOfBookings from '../components/dashboard/RateOfBookings';
import globalStyles from '../styles';
import Data from '../data';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import BookingTopSection from '../components/dashboard/BookingTopSection';

// h3 style={globalStyles.navigation}>Application / Bookings</h3>

const BookingPage = () => {

  const styles = {
    selectField: {
      
    },
  };

  return (
    <div>
       <div className="row" />
       <BookingTopSection />
       <div className="row">  
        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 m-b-15">
          <SelectField
          style= {styles.selectField}
          floatingLabelText="Select Filter"
          floatingLabelStyle={{color:"black",fontSize: 20}}
          labelStyle={{color:globalStyles.appColors.cyan}}
          iconStyle={{fill:"black"}}
          underlineStyle={{borderColor:globalStyles.appColors.grey}}
          fullWidth= {true}
          value={"month"}
        >
          <MenuItem value={"month"}  primaryText={"By Month"} />
          <MenuItem value={"week"} primaryText={"By Week"} />
          <MenuItem value={"year"} primaryText={"By Year"} />
        </SelectField>
          <MonthlySales data={Data.dashBoardPage.monthlySales}/>
        </div>
        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 m-b-15">
        <SelectField
          floatingLabelText="Select Filter"
           floatingLabelStyle={{color:"black",fontSize: 20}}
          labelStyle={{color:globalStyles.appColors.cyan}}
          iconStyle={{fill:"black"}}
          underlineStyle={{borderColor:globalStyles.appColors.grey}}
          fullWidth= {true}
          value={"month"}
        >
          <MenuItem value={"month"}  primaryText={"By Month"} />
          <MenuItem value={"week"} primaryText={"By Week"} />
          <MenuItem value={"year"} primaryText={"By Year"} />
        </SelectField>
          <RateOfBookings />
        </div>
      </div>
    </div>
  );
};

export default BookingPage;
