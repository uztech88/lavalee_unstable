
export const cssrDetails = [{
	name: 'cssr1',
	email: 'cssr1@test.com',
	phone_number: '021-6778987',
	password: 'cssr1',
},
{
	name: 'cssr2',
	email: 'cssr2@test.com',
	phone_number: '021-6778987',
	password: 'cssr2',
}];

export const operationalDetails = [{
	name: 'op1',
	email: 'op1@test.com',
	phone_number: '021-6778987',
	password: 'op1',
},
{
	name: 'op2',
	email: 'op2@test.com',
	phone_number: '021-6778987',
	password: 'op2',
}];