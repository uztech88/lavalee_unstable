import { LOGIN_SUCCESS, LOGIN_VALIDATION_FAILED,
  REMOVE_FIELD_ERROR, LOGIN_FAILED, DIALOG_CLOSE,
  LOG_OUT_SUCCESS, LOG_OUT_FAILED } from '../constants/authTypes';
import dialogTypes from '../constants/dialogTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';


export default function loginReducer(state = initialState.loginReducer, action) {
  
  switch (action.type) {
    case LOGIN_SUCCESS:
      return objectAssign({}, state, {
        isLoggedIn: true,
      });

    case LOGIN_VALIDATION_FAILED:
      return objectAssign({}, state, {
        errors: action.errors
      });

    case REMOVE_FIELD_ERROR:
      let res = { errors: objectAssign({}, state.errors) };
      res.errors[action.name] = '';
      return objectAssign({}, state, res);

    case LOGIN_FAILED:
     console.log("login failed reducer fired");
      return objectAssign({}, state, {
        isError: true,
        errorMessage: action.err,
      });

    case LOG_OUT_SUCCESS:
      return objectAssign({}, state, {
        isLoggedIn: false,
      });

    case LOG_OUT_FAILED:
      console.log("logout failed reducer fired");
      return objectAssign({}, state, {
        isError: true,
        errorMessage: action.err,
      });

    case dialogTypes.DIALOG_CLOSE:
      return objectAssign({}, state, {
        isError: false,
        errorMessage: '',
      });

    default:
      return state;
  }
}
