import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import redirectionReducer from './redirectionReducer';
import userReducer from './userReducer';
import {routerReducer} from 'react-router-redux';

const rootReducer = combineReducers({
  loginReducer,
  redirectionReducer,
  userReducer,
  routing: routerReducer
});

export default rootReducer;
