import { LOADER_SHOW, LOADER_HIDE } from '../constants/loaderTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function loaderReducer(state = initialState.loader, action) {

  switch (action.type) {
    case loaderTypes.LOADER_SHOW:
      console.log("loader reducer LOADER_SHOW fired");
      return objectAssign({}, state, { show: true });

    case loaderTypes.LOADER_HIDE:
      console.log("loader reducer LOADER_HIDE fired");
      return objectAssign({}, state, { show: false });

    default:
      console.log(state)
      return state;
  }
}
