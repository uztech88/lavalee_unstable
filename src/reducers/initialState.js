export default {
	redirectionReducer: {
		currentUrl: '',
		redirectTo: '',
	},	
	userReducer: {
		user: {
			id: '',
			email: '',
			firstName: '',
			lastName: '',
			phoneNumber: '',
			referralCode: '',
			token: '',
		},
		isCSSR: false,
		isOp: true,
	},
	loginReducer: {
		errors: {
		},
		isLoggedIn: false,
		isError: false,
		errorMessage: '',
	},
	
};
