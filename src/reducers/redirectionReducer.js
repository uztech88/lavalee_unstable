import redirectionTypes from '../constants/redirectionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I'm using Object.assign to create a copy of current state
// and update values on the copy.
export default function redirectionReducer(state = initialState.redirectionReducer, action) {

  switch (action.type) {
    case redirectionTypes.REDIRECT_TO:
      console.log("redirection reducer REDIRECT_TO fired");
      return objectAssign({},state,{redirectTo:action.uri});

    case redirectionTypes.SET_CURRENT_URI:
      console.log("redirection reducer SET_CURRENT_URI fired");
      return objectAssign({},state,{currentUrl:action.uri});
        
    default:
      return state;
  }
}
