import { SAVE_USER ,UPDATE_USER,
  REMOVE_USER } from '../constants/userTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

export default function userReducer(state = initialState.userReducer, action) {

  switch (action.type) {

    case SAVE_USER:
      console.log("loader reducer SAVE_USER fired");
      return objectAssign({}, state, action.data);
 
    case UPDATE_USER:
      console.log("loader reducer UPDATE_USER fired");
      return objectAssign({}, state, action.data);

    case REMOVE_USER:
      console.log("loader reducer REMOVE_USER fired");
      return objectAssign({}, state,
        initialState.userReducer);

    default:
      return state; 
  }

}
