import dialogTypes from '../constants/dialogTypes';

export const dialogOpen = (data) => {
  return {
    type: dialogTypes.DIALOG_OPEN,
    data
  };
};

export const dialogClose = (fields) => {
  return {
    type: dialogTypes.DIALOG_CLOSE,
    fields
  };
};
