import loaderTypes from '../constants/loaderTypes';

export const loaderShow = () => {
  return {
    type: dialogTypes.LOADER_SHOW,
  };
};

export const loaderHide = () => {
  return {
    type: dialogTypes.LOADER_HIDE,
  };
};
