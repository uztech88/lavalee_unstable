import redirectionTypes from '../constants/redirectionTypes';
import objectAssign from 'object-assign';

export const redirectTo = (uri) => {
  return {
    type: redirectionTypes.REDIRECT_TO,
    uri
  };
};

export const setCurrentUri = (uri) => {
  return {
    type: redirectionTypes.SET_CURRENT_URI,
    uri
  };
};

export const isLogged = (status) => {
  return {
    type: redirectionTypes.SET_LOGGED_STATUS,
    status
  };
};
