import { MAIL_SENT, MAIL_FAILED } from '../constants/recoveryTypes';

export const mailSent = () => {
  return {
    type: MAIL_SENT,
  };
};

export const mailFailed = () => {
  return {
    type: MAIL_FAILED,
  };
};
