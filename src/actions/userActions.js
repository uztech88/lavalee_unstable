import { SAVE_USER, UPDATE_USER, REMOVE_USER } from '../constants/userTypes';

export const saveUser = (data) => {
  return {
    type: SAVE_USER,
    data
  };
};

export const updateUser = (data) => {
  return {
    type: UPDATE_USER,
    data
  };
};

export const removeUser = () => {
  return {
    type: REMOVE_USER,
  };
};
