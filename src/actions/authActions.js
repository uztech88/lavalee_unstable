import { LOGIN_SUCCESS, LOGIN_VALIDATION_FAILED,
  REMOVE_FIELD_ERROR, LOGIN_FAILED, DIALOG_CLOSE, LOG_OUT_SUCCESS,
  LOG_OUT_FAILED } from '../constants/authTypes';
import { apiLogin, apiLogout } from '../sources/authSource';

export const loginSuccess = (data) => {
  return {
    type: LOGIN_SUCCESS,
    data
  };
};

export const loginFailed = (err) => {
  return {
    type: LOGIN_FAILED,
    err
  };
};

export const removeFieldError = (name) => {
  return {
    type: REMOVE_FIELD_ERROR,
    name
  };
};


export const loginValidationFailed = (errors) => {
  return {
    type : LOGIN_VALIDATION_FAILED,
    errors
  };
};

export const logoutSuccess = () => {
  return{
    type : LOG_OUT_SUCCESS,
  };
};

export const logoutFailed = () => {
  return{
    type : LOG_OUT_FAILED,
  };
};

//Async Action
export const submitLogin = (form) => {
  return apiLogin(form);
};

export const submitLogout = (token) => {
  return apiLogout(token);
};
