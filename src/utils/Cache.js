import CryptoJS from 'crypto-js';
import { ENCRYPTION_KEY } from './Constants';

import { localStorage, sessionStorage, isLocalStorageSupported } from './storage/index';

let instance = null;
let initialSessionReceived = null;

class Cache {

  constructor() {
    if (!instance) {
      instance = this;

      // checking if storage available
      if (typeof(Storage) !== 'undefined') {
        this.storageAvailable = isLocalStorageSupported();
        this.askForSession();
      } else {
        this.storageAvailable = false;
      }

      this.inMemorySession = {};
      this.inMemoryLocal = {};
    }
    return instance;
  }

  sessionExists(key) {
    if (this.storageAvailable) {
      return sessionStorage.getItem(key) !== null;
    }
    return (this.inMemorySession[key] !== null && this.inMemorySession[key] !== undefined);
  }

  getSession(key, isJson = false, decrypt = true) {
    let data = this.storageAvailable ? sessionStorage.getItem(key) : this.inMemorySession[key];

    if (!!data) {
      if (decrypt) {
        const bytes = CryptoJS.AES.decrypt(data.toString(), ENCRYPTION_KEY);
        data = bytes.toString(CryptoJS.enc.Utf8);
      }
      if (isJson) {
        data = JSON.parse(data);
      }
    }
    return data;
  }

  replaceSession(session) {
    const s = JSON.parse(session);
    Object.keys(s).forEach((i) => {
      sessionStorage.setItem(i, s[i]);
    });

    Object.keys(sessionStorage).forEach((i) => {
      if (!s[i]) {
        sessionStorage.removeItem(i);
      }
    });
  }

  getInitialSession(session) {
    if (initialSessionReceived) return;
    this.replaceSession(session);
    initialSessionReceived = true;
    /* the line below was commented because this was causing redirection
    errors when the user opens more than one window */
    // location.hash = 'dashboard';
  }

  shareInitialSession() {
    this.triggerStorageEvent('_Initial_Session_', JSON.stringify(sessionStorage));
  }

  shareSession() {
    this.triggerStorageEvent('_Shared_Session_', JSON.stringify(sessionStorage));
  }

  askForSession() {
    this.triggerStorageEvent('_Get_Shared_Session_', '1');
  }

  saveSession(key, dataParam, isJson = false, encrypt = true) {

    let data = dataParam;
    if (isJson) {
      data = JSON.stringify(data);
    }
    if (encrypt) {
      data = CryptoJS.AES.encrypt(data, ENCRYPTION_KEY).toString();
    }

    if (this.storageAvailable) {
      sessionStorage.setItem(key, data);
      this.shareSession();
    } else {
      this.inMemorySession[key] = data;
    }
  }

  removeSession(key) {
    if (this.storageAvailable) {
      sessionStorage.removeItem(key);
      this.shareSession();
    } else {
      delete this.inMemorySession[key];
    }
  }

  // localStorage methods
  localExists(key) {
    if (this.storageAvailable) {
      return localStorage.getItem(key) != null;
    }
    return this.inMemoryLocal[key] != null;
  }

  getLocal(key, isJson = false, decryptParam = true) {
    let data;
    let decrypt = decryptParam;

    if (this.storageAvailable) {
      data = localStorage.getItem(key);
    } else {
      data = this.inMemoryLocal[key];
    }

    if (!data) {
      data = '{}';
      decrypt = false;
    }

    // TODO: temporary isJson check. remove it later
    if (decrypt || !this.isJson(data)) {
      const bytes = CryptoJS.AES.decrypt(data.toString(), ENCRYPTION_KEY);
      data = bytes.toString(CryptoJS.enc.Utf8);
    }
    if (isJson) {
      data = (!data) ? [] : JSON.parse(data);
    }
    return data;
  }

  saveLocal(key, dataParam, isJson = false, encrypt = true) {
    let data = dataParam;
    if (isJson) {
      data = JSON.stringify(data);
    }
    if (encrypt) {
      data = CryptoJS.AES.encrypt(data, ENCRYPTION_KEY).toString();
    }

    if (this.storageAvailable) {
      localStorage.setItem(key, data);
    } else {
      this.inMemoryLocal[key] = data;
    }
  }

  removeLocal(key) {
    if (this.storageAvailable) {
      localStorage.removeItem(key);
    } else {
      delete this.inMemoryLocal[key];
    }
  }

  triggerStorageEvent(key, data) {
    if (this.storageAvailable) {
      localStorage.setItem(key, data);
      localStorage.removeItem(key);
    }
  }


  // TODO: its a temporary method will remove it later
  isJson(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
}

const cache = new Cache(); // singleton

module.exports = cache;
