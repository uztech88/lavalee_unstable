 
export function LoginRedirection (nextState, replace, state) {
	if(!(state && state.loginReducer.isLoggedIn)) {
    replace({ pathname: '/login' });
  }
};

export function PanelRedirection (nextState, replace, state) {
	if((state && state.loginReducer.isLoggedIn)) {
    if(state && state.userReducer.isCSSR){
      replace({ pathname: '/cssr_panel' });
    }else{
      replace({ pathname: '/op_panel' });
    }
  }
};
