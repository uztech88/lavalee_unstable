import InmemoryStorage from './InmemoryStorage';

describe('on new in memory storage', () => {
  let inMemory;

  beforeEach(() => {
    inMemory = new InmemoryStorage();
  });

  it('must initialize empty storage', () => {
    expect(Object.keys(inMemory.storage)).toHaveLength(0);
  });
});

describe('on set new item', () => {
  let inMemory, value;

  beforeEach(() => {
    inMemory = new InmemoryStorage();
    value = 1;
  });

  it('must change storage', () => {
    inMemory.setItem('item', value);
    expect(inMemory.storage.item).toBe(value);
  });
});

describe('on get an item', () => {
  let inMemory, value;
  
  beforeEach(() => {
    inMemory = new InmemoryStorage();
    value = 1;
  });

  it('must be returned when setted', () => {
    inMemory.setItem('item', value);
    expect(inMemory.getItem('item')).toBe(value);
  });

  it('must return undefined when not setted', () => {
    expect(inMemory.getItem('item')).toBeUndefined();
  });
});

describe('on remove an item', () => {
  let inMemory, value;
  
  beforeEach(() => {
    inMemory = new InmemoryStorage();
    value = 1;
  });

  it('must be deleted', () => {
    inMemory.setItem('item', value);
    inMemory.removeItem('item');
    expect(inMemory.storage.item).toBeUndefined();
  });
});

describe('on clear', () => {
  let inMemory, value;
  
  beforeEach(() => {
    inMemory = new InmemoryStorage();
    value = 1;
  });

  it('must empty storage', () => {
    inMemory.setItem('item', value);
    inMemory.clear();
    expect(inMemory.storage.item).toBeUndefined();
    expect(Object.keys(inMemory.storage)).toHaveLength(0);
  });
});
