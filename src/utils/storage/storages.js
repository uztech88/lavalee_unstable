import InmemoryStorage from './InmemoryStorage';

export const isLocalStorageSupported = () => {
  const testKey = 'localStorage';
  const storage = global.localStorage;
  try {
    storage.setItem(testKey, '1');
    storage.removeItem(testKey);
    return true;
  } catch (e) {
    return false;
  }
};

const isStorageAvailable = isLocalStorageSupported();

export const localStorage = isStorageAvailable ? global.localStorage : new InmemoryStorage();
export const sessionStorage = isStorageAvailable ? global.sessionStorage : new InmemoryStorage();
