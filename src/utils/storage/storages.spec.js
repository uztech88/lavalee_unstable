import {
  isLocalStorageSupported,
} from './storages';
import InmemoryStorage from './InmemoryStorage';

describe('when local storage is not supported', () => {
  it('must return false', () => {
    expect(isLocalStorageSupported()).toBeFalsy();
  });
});

describe('when local storage is supported', () => {
  it('must return true', () => {
    global.localStorage = new InmemoryStorage(); // avoid change global scope
    expect(isLocalStorageSupported()).toBeTruthy();
    delete global.localStorage; // always remenber to rollback change on global scope
  });
});
