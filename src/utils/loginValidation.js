export default function checkLoginValidation(form){
	const emailResponse = validateEmail(form.target.email.value);
	const passwordResponse = validatePassword(form.target.password.value);
	let res = {
		errors: {
		},
	};
	if(emailResponse.isError){
		res.isError = emailResponse.isError
		res.errors["email"] = emailResponse.message;
	}
	if(passwordResponse.isError){
		res.isError = passwordResponse.isError
		res.errors["password"] = passwordResponse.message;
	}
	return res;
};

function validatePassword(password){
	let res = setResponse();
	return !password ? setResponse("Password is required") : res;
}

function validateEmail(email) {
	let res = setResponse();
	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(!email) {
		res = setResponse("Email is required");
	}else {
		if(!re.test(email)) {
			res = setResponse("Email is invalid");
		}
	}
	return res;
} 

function setResponse(errorMessage){
	return {
		isError: errorMessage ? true : false,
		message : errorMessage
	};
};
