import Cache from './Cache';
import { STORE_KEY } from './Constants';

export function getStoreFromLocalStorage() {
	if (Cache.localExists(STORE_KEY)){
		return Cache.getLocal(STORE_KEY, true);
	}
	return {};
};

export function saveStoreToLocalStorage(datapacket){
	Cache.saveLocal(STORE_KEY, datapacket, true);
};

export function removeStoreFromLocalStorage(){
	Cache.removeLocal(STORE_KEY);
};

export function getAuthData(state){
	return {
		userReducer: state.userReducer,
		loginReducer: {
			isLoggedIn: state.loginReducer.isLoggedIn,
		},
	}
};
