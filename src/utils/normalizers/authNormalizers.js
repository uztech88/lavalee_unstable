export function normalizeLoginData(form){
  return {
    "email": form.target.email.value,
    "password": form.target.password.value
  }
};

export function normalizeUserData(data){
  return {
  	user: {
      "id": data.responseBody.id,
    	"email": data.responseBody.email,
    	"firstName": data.responseBody.firstName,
    	"lastName": data.responseBody.lastName,
    	"phoneNumber": data.responseBody.phoneNumber,
    	"referralCode": data.responseBody.referralCode,
    	"token": data.responseHeader.token,
	  },
  	isCSSR: true,
  	isOp: false,
  }
};
