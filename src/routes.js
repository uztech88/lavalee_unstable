import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { LoginRedirection, PanelRedirection } from './utils/authentication';
import App from './containers/App';
import NotFoundPage from './containers/NotFoundPage.js';
import LoginPage from './containers/LoginPage';
import ForgotPasswordPage from './containers/ForgotPasswordPage';
import RecoveryMailPage from './containers/RecoveryMailPage';
import SignupPage from './containers/SignupPage';
import FormPage from './containers/FormPage';
import TablePage from './containers/TablePage';
import ValetPage from './containers/ValetPage';
import ServicesPage from './containers/ServicesPage';
import ServiceZonePage from './containers/ServiceZonePage';
import BookingPage from './containers/BookingPage';

const Routes = (store) => {

  const redirectToPanelIfLogin = (nextState, replace) => {
    const state = store.getState();
    PanelRedirection(nextState, replace, state);
  };

  const redirectToLoginIfLogout = (nextState, replace) => {
    const state = store.getState();
    LoginRedirection(nextState, replace, state);
  };

  const CssrRoutes = () => {
    return(
      <Route path="/op_panel" component={App} onEnter = {redirectToLoginIfLogout} >
        <IndexRoute component={BookingPage}/>
        <Route path="valets" component={ValetPage}/>
        <Route path="bookings" component={BookingPage}/>
        <Route path="services" component={ServicesPage}/>
        <Route path="s_zones" component={ServiceZonePage}/>
        <Route path="*" component={NotFoundPage}/>
      </Route>
      );
  }

  const OpRoutes = () => {
    return(
      <Route path="/cssr_panel" component={App} onEnter = {redirectToLoginIfLogout} >
        <IndexRoute component={BookingPage}/>
        <Route path="valets" component={ValetPage}/>
        <Route path="bookings" component={BookingPage}/>
        <Route path="services" component={ServicesPage}/>
        <Route path="s_zones" component={ServiceZonePage}/>
      </Route>
      );
  }

  return(
      <Route>
        <Route path="/" onEnter = {redirectToLoginIfLogout} />
        <Route path="login" component={LoginPage}
        onEnter = {redirectToPanelIfLogin} />
        <Route path="forgot_password" component={ForgotPasswordPage}
        onEnter = {redirectToPanelIfLogin} />
        <Route path="recovery_mail" component={RecoveryMailPage}
        onEnter = {redirectToPanelIfLogin} />
        {CssrRoutes()}
        {OpRoutes()}
      </Route>
    );
};

export default Routes;
