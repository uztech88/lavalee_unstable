export const loginUrls = {
	DEV: 'http://52.40.168.11:8080/LVDEV/v1/authenticate/login',
	QA: 'http://35.162.128.172:8080/LVQA/v1/authenticate/login', 	
};
export const signUpUrls = {
	DEV: 'http://52.40.168.11:8080/LVDEV/v1/registration/signup',
	QA: 'http://35.162.128.172:8080/LVQA/v1/registration/signup',  
};
export const verifyUrls = {
	DEV: 'http://52.40.168.11:8080/LVDEV/v1/registration/verify',
	QA: 'http://35.162.128.172:8080/LVQA/v1/registration/verify',  
};
export const changePasswordsUrls = {
	DEV: 'http://52.40.168.11:8080/LVDEV/v1/user/changePassword',
	QA: 'http://35.162.128.172:8080/LVQA/v1/user/changePassword',  
};
export const logoutUrls = {
	DEV: 'http://52.40.168.11:8080/LVDEV/v1/user/logout',
	QA: 'http://35.162.128.172:8080/LVQA/v1/user/logout', 	
};