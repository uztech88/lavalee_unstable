/* eslint-disable import/default */

import React from 'react';
import { render } from 'react-dom';
import { browserHistory } from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Root from './root';
require('./favicon.ico');
import './styles.scss';
import 'font-awesome/css/font-awesome.css';
import 'flexboxgrid/css/flexboxgrid.css';
import configureStore from './store/configureStore';
import { syncHistoryWithStore } from 'react-router-redux';
import initialState from './reducers/initialState';

const store = configureStore(initialState);

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);

injectTapEventPlugin();

render(
  <Root store={store} history={browserHistory} />,
  document.getElementById('app')
);
