import {createStore, compose, applyMiddleware} from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/index';
import objectAssign from 'object-assign';
import { getStoreFromLocalStorage,
  saveStoreToLocalStorage, getAuthData } from '../utils/storeStorage';
import throttle from 'lodash/throttle';

function configureStoreProd(initialState) {
  const middlewares = [
    thunk,
  ];

  const existedStore = getStoreFromLocalStorage();
  initialState = objectAssign({},initialState,existedStore); 

  const store = createStore(rootReducer, initialState, compose(
    applyMiddleware(...middlewares)
    )
  );

  store.subscribe(throttle(() => {
    console.log("written in localStorage");
    saveStoreToLocalStorage(getAuthData(store.getState()));
  }, 1000));
  
  return store;
}

function configureStoreDev(initialState) {
   
  const existedStore = getStoreFromLocalStorage();
  initialState = objectAssign({},initialState,existedStore); 
  const middlewares = [
    reduxImmutableStateInvariant(),
    thunk,
  ];

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
  const store = createStore(rootReducer, initialState, composeEnhancers(
    applyMiddleware(...middlewares)
    )
  );

  store.subscribe(throttle(() => {
    console.log("written in localStorage");
    saveStoreToLocalStorage(getAuthData(store.getState()));
  }, 1000));

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default; // eslint-disable-line global-require
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

const configureStore = process.env.NODE_ENV === 'production' ? configureStoreProd : configureStoreDev;

export default configureStore;