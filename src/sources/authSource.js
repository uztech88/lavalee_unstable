import { saveUser, removeUser } from '../actions/userActions';
import { loginFailed, loginSuccess,
  logoutSuccess, logoutFailed } from '../actions/authActions';
import { defaultHeaders } from '../constants/defaultHeaders';
import { loginRequestBody } from '../constants/defaultRequestData';
import { loginUrls, logoutUrls } from '../constants/urls';
import { normalizeLoginData,
  normalizeUserData } from '../utils/normalizers/authNormalizers';
import initialState from '../reducers/initialState';
import objectAssign from 'object-assign';
import Request from 'superagent';

export const apiLogin = (form) => {
  let requestData = objectAssign({}, loginRequestBody,
    normalizeLoginData(form));
  const url = loginUrls.QA;
  console.log("api request fired");
  return (dispatch) => {
     return Request
              .post(url)
              .set(defaultHeaders)
              .send(requestData)
              .type('application/json')
              .end((err, res) => {
                if (err) {
                  console.log(err);
                  dispatch(loginFailed(err));
                 } else {
                  const result = JSON.parse(res.text);
                  if (result.responseHeader.isError){
                    dispatch(loginFailed(result.responseHeader.message));
                  }else{
                    dispatch(loginSuccess(result));
                    dispatch(saveUser(normalizeUserData(result)));
                  }
              }
        });
    };
};

export const apiLogout = (token) => {
  const url = logoutUrls.QA;
  console.log("logout api request fired");
  return (dispatch) => {
    console.log(url);
     return Request
              .get(url)
              .set(defaultHeaders)
              .query({ token })
              .accept('application/json')
              .end((err, res) => {
                if (err) {
                  console.log(err);
                  dispatch(logoutFailed(err));
                 } else {
                  const result = JSON.parse(res.text);
                  if (result.responseHeader.isError){
                    dispatch(logoutFailed(result.responseHeader.message));
                  }else{
                    dispatch(logoutSuccess());
                    dispatch(removeUser(initialState.userReducer));
                  }
              }
        });
    };
};
