import React from 'react';
import Assessment from 'material-ui/svg-icons/action/assessment';
import GridOn from 'material-ui/svg-icons/image/grid-on';
import PermIdentity from 'material-ui/svg-icons/action/perm-identity';
import Web from 'material-ui/svg-icons/av/web';
import {grey900, grey700, orange800, red500} from 'material-ui/styles/colors';
import ExpandLess from 'material-ui/svg-icons/navigation/expand-less';
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';

const data = {
  user: {
    username: "Omer (Ops)",
  },
  menus: [
    { text: 'Valets', icon: <Assessment/>, link: '/op_panel/valets' },
    { text: 'Service Zones', icon: <Assessment/>, link: '/op_panel/s_zones' },
    { text: 'Bookings', icon: <Web/>, link: '/op_panel/bookings' },
  ],
  tablePage: {
    items: [
      {id: 1, name: 'Product 1', price: '$50.00', category: 'Category 1'},
      {id: 2, name: 'Product 2', price: '$150.00', category: 'Category 2'},
      {id: 3, name: 'Product 3', price: '$250.00', category: 'Category 3'},
      {id: 4, name: 'Product 4', price: '$70.00', category: 'Category 4'},
      {id: 5, name: 'Product 5', price: '$450.00', category: 'Category 5'},
      {id: 6, name: 'Product 6', price: '$950.00', category: 'Category 6'},
      {id: 7, name: 'Product 7', price: '$550.00', category: 'Category 7'},
      {id: 8, name: 'Product 8', price: '$750.00', category: 'Category 8'}
    ]
  },
  dashBoardPage: {
    recentProducts: [
      {id: 1, title: 'Zone1', services: [
        {title: "Car Washing", status: "availaible"},
        {title: "Fuel Refiling", status: "availaible"},
        {title: "Car Repairing", status: "none"},
        ]},
      {id: 2, title: 'Zone2', services: [
        {title: "Car Washing", status: "availaible"},
        {title: "Fuel Refiling", status: "availaible"},
        {title: "Car Repairing", status: "none"},
        ]},
    ],
    TopValets: [
      {id: 1, title: 'Valet1', rating: 4.9},
      {id: 2, title: 'Valet10', rating: 4.9},
      {id: 3, title: 'Valet3', rating: 4.8},
      {id: 4, title: 'Valet8', rating: 4.7},
      {id: 5, title: 'Valet9', rating: 4.0},
    ],
    monthlySales: [
      {name: 'Jan', bookings: 3700},
      {name: 'Feb', bookings: 3000},
      {name: 'Mar', bookings: 2000},
      {name: 'Apr', bookings: 2780},
      {name: 'May', bookings: 2000},
      {name: 'Jun', bookings: 1800},
      {name: 'Jul', bookings: 2600},
      {name: 'Aug', bookings: 2900},
      {name: 'Sep', bookings: 3500},
      {name: 'Oct', bookings: 3000},
      {name: 'Nov', bookings: 2400},
      {name: 'Dec', bookings: 2780}
    ],
    newOrders: [
      {pv: 2400},
      {pv: 1398},
      {pv: 9800},
      {pv: 3908},
      {pv: 4800},
      {pv: 3490},
      {pv: 4300}
    ],
    ValetsStatus: [
      {name: 'Busy', value: 7, color: grey900, icon: <ExpandMore/>},
      {name: 'Availaible', value: 2, color: grey700, icon: <ChevronRight/>},
      {name: 'Offline', value: 1, color: red500, icon: <ExpandLess/>}
    ],
    ParkingLotsInZones: [
      {name: 'Zone 1', value: 7, color: grey900, icon: <ChevronRight/>},
      {name: 'Zone 2', value: 6, color: orange800, icon: <ChevronRight/>},
      {name: 'Zone 3', value: 3, color: red500, icon: <ChevronRight/>}
    ],
  },
  ZonesComparisions: [
      {name: 'Mon', Zone1: 4000, Zone2: 2400, Zone3: 2400},
      {name: 'Tue', Zone1: 3000, Zone2: 1398, Zone3: 2210},
      {name: 'Wed', Zone1: 2000, Zone2: 9800, Zone3: 2290},
      {name: 'Thursday', Zone1: 2780, Zone2: 3908, Zone3: 2000},
      {name: 'Friday', Zone1: 1890, Zone2: 4800, Zone3: 2181},
      {name: 'Saturday', Zone1: 2390, Zone2: 3800, Zone3: 2500},
      {name: 'Sunday', Zone1: 3490, Zone2: 4300, Zone3: 2100},
  ],
  Zones:["Zone1","Zone2","Zone3"],
  ZonesColors:[grey900,orange800,red500],
  CoreData: {
    ServiceZones: [
    {
      name: "Zone 1",
      color: grey900,
      parkingLots: [
      {
        id: 1,
        name: "ParkingLot1",
        position:{
          lat: 29.3117,
          lng: 47.4818,
        },
        parkingSpace: 10,
        totalCarsParked: 5,
        ServicesStatus: {
          isPending: 3,
          isProgress: 1,
          isCompleted: 1,
        },
        time: {
          opening: "9:00AM",
          closing: "7:00PM",
        },
        status: "open",
        services: [
        {title: "Car Washing", status: "availaible"},
        {title: "Fuel Refiling", status: "availaible"},
        {title: "Car Repairing", status: "none"},
        ],
      },
      ],
      areaPolygons: {
        lat: 29.3117,
        lng: 47.4818,
        radius: 5000,
      },
      valets: [
       {
        id: 1,
        name: "Valet1",
        status: "Active",
        position:{
          lat: 29.3117,
          lng: 47.4818,
        },
        rating: 4.8,
       },
       {
        id: 2,
        name: "Valet2",
        status: "Active",
        position:{
          lat: 29.3117+0.01,
          lng: 47.4818+0.01,
        },
        rating: 4.1,
       }
      ],
    },
    {
      name: "Zone 2",
      color: orange800,
      parkingLots: [
      {
        id: 1,
        name: "ParkingLot1",
        position:{
          lat: 29.3117,
          lng: 47.4818,
        },
        parkingSpace: 10,
        totalCarsParked: 5,
        ServicesStatus: {
          isPending: 3,
          isProgress: 1,
          isCompleted: 1,
        },
        time: {
          opening: "9:00AM",
          closing: "7:00PM",
        },
        status: "Available",
        services: [
        {title: "Car Washing", status: "availaible"},
        {title: "Fuel Refiling", status: "availaible"},
        {title: "Car Repairing", status: "none"},
        ],
      },
      ],
      areaPolygons: {
        lat: 29.3117+0.09,
        lng: 47.4818+0.09,
        radius: 5000,
      },
      valets: [
       {
        id: 1,
        name: "Valet1",
        status: "Active",
        img: "https://s-media-cache-ak0.pinimg.com/736x/af/5a/ac/af5aac155845e889523d038e29da8684.jpg",
        position:{
          lat: 29.3117,
          lng: 47.4818,
        },
        rating: 4.8,
       },
       {
        id: 2,
        name: "Valet2",
        img: "https://s-media-cache-ak0.pinimg.com/736x/af/5a/ac/af5aac155845e889523d038e29da8684.jpg",
        status: "Active",
        position:{
          lat: 29.3117+0.01,
          lng: 47.4818+0.01,
        },
        rating: 4.1,
       }
      ],
    },
    {
      name: "Zone 3",
      color: red500,
      parkingLots: [
      {
        id: 1,
        name: "ParkingLot1",
        position:{
          lat: 29.3117,
          lng: 47.4818,
        },
        parkingSpace: 10,
        totalCarsParked: 5,
        ServicesStatus: {
          isPending: 3,
          isProgress: 1,
          isCompleted: 1,
        },
        time: {
          opening: "9:00AM",
          closing: "7:00PM",
        },
        status: "Available",
        services: [
        {title: "Car Washing", status: "available"},
        {title: "Fuel Refiling", status: "available"},
        {title: "Car Repairing", status: "not available"},
        ],
      },
      ],
      areaPolygons: {
        lat: 29.3117,
        lng: 47.4818+0.19,
        radius: 5000,
      },
      valets: [
       {
        id: 1,
        name: "Valet1",
        status: "Active",
        position:{
          lat: 29.3117,
          lng: 47.4818,
        },
        rating: 4.8,
       },
       {
        id: 2,
        name: "Valet2",
        status: "Active",
        position:{
          lat: 29.3117+0.01,
          lng: 47.4818+0.01,
        },
        rating: 4.1,
       }
      ],
    },
    ]
  }
};

export default data;
