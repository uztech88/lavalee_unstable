import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import Routes from './routes';
import { Router } from 'react-router';
import LoginPage from './containers/LoginPage';

export default class Root extends Component {
  render() {
    const { store, history } = this.props;
    console.log("root changed");
    return (
      <Provider store={store}>
        <Router history={history} routes={Routes(store)} />
      </Provider>
    );
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};